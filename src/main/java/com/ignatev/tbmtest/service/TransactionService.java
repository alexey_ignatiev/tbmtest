package com.ignatev.tbmtest.service;

import com.ignatev.tbmtest.entity.Account;
import com.ignatev.tbmtest.entity.Transaction;
import com.ignatev.tbmtest.repository.AccountRepository;
import com.ignatev.tbmtest.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;

@Service
@Transactional
public class TransactionService {

    private final AccountRepository accountRepository;

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public Transaction transfer(String fromId, String toId, Double money) {
        if (money <= 0.) {
            throw new RuntimeException("Can't transfer negative value");
        }
        Account accountFrom = accountRepository.findById(UUID.fromString(fromId)).orElseThrow(() -> new RuntimeException("Can't find account with id =" + fromId));
        BigDecimal balance = accountFrom.getAmount();
        BigDecimal balanceAfterOperation = balance.add(BigDecimal.valueOf(money).negate());
        if (balanceAfterOperation.compareTo(BigDecimal.ZERO) < 0) {
            throw new RuntimeException("Not enough money in the account id=" + accountFrom.getId());
        }
        accountFrom.setAmount(balanceAfterOperation);
        accountFrom = accountRepository.save(accountFrom);
        Account accountTo = accountRepository.findById(UUID.fromString(toId)).orElseThrow(() -> new RuntimeException("Can't find account with id =" + toId));
        BigDecimal toBalance = accountTo.getAmount();
        BigDecimal newToBalance = toBalance.add(BigDecimal.valueOf(money));
        accountTo.setAmount(newToBalance);
        accountTo = accountRepository.save(accountTo);
        Transaction transaction = new Transaction(accountFrom, accountTo, BigDecimal.valueOf(money));
        return transactionRepository.save(transaction);
    }

}
