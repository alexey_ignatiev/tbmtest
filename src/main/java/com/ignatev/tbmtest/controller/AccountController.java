package com.ignatev.tbmtest.controller;

import com.ignatev.tbmtest.entity.Account;
import com.ignatev.tbmtest.entity.Client;
import com.ignatev.tbmtest.repository.AccountRepository;
import com.ignatev.tbmtest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.UUID;

@Controller
@RequestMapping("/account")
public class AccountController {

    private final AccountRepository accountRepository;

    private final ClientRepository clientRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository, ClientRepository clientRepository) {
        this.accountRepository = accountRepository;
        this.clientRepository = clientRepository;
    }

    @PostMapping("/{clientId}/add")
    public String addAccount(@PathVariable String clientId, @RequestParam Double amount) {
        Client client = clientRepository.findById(UUID.fromString(clientId)).orElseThrow(() -> new RuntimeException("Client with id = " + clientId + " not found"));
        Account account = new Account(new BigDecimal(amount), client);
        accountRepository.save(account);
        return "redirect:/client/{clientId}";
    }

}
