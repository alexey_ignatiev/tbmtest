package com.ignatev.tbmtest.controller;

import com.ignatev.tbmtest.entity.Account;
import com.ignatev.tbmtest.entity.Transaction;
import com.ignatev.tbmtest.repository.AccountRepository;
import com.ignatev.tbmtest.repository.TransactionRepository;
import com.ignatev.tbmtest.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    private final TransactionRepository transactionRepository;

    private final AccountRepository accountRepository;

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(AccountRepository accountRepository, TransactionRepository transactionRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.transactionService = transactionService;
    }

    @GetMapping
    public String transaction(Model model) {
        model.addAttribute("transactions", transactionRepository.findAll());
        model.addAttribute("accounts", accountRepository.findAll());
        model.addAttribute("transaction", new Transaction());
        return "transaction";
    }

    @PostMapping
    public String transfer(@RequestParam String from, @RequestParam String to, @RequestParam Double amount) {
        transactionService.transfer(from, to, amount);
        return "redirect:/transaction";
    }

}
