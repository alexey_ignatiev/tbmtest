package com.ignatev.tbmtest.controller;

import com.ignatev.tbmtest.entity.Account;
import com.ignatev.tbmtest.entity.Client;
import com.ignatev.tbmtest.repository.AccountRepository;
import com.ignatev.tbmtest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/client")
public class ClientController {

    private final ClientRepository clientRepository;

    private final AccountRepository accountRepository;

    @Autowired
    public ClientController(AccountRepository accountRepository, ClientRepository clientRepository) {
        this.accountRepository = accountRepository;
        this.clientRepository = clientRepository;
    }

    @PostMapping("/add")
    public String addClient(@RequestParam String name, @RequestParam String address) {
        Client client = new Client(name, address);
        clientRepository.save(client);
        return "redirect:/";
    }

    @GetMapping("/{id}")
    public String client(@PathVariable String id, Model model) {
        UUID clientId = UUID.fromString(id);
        Client client = clientRepository.findById(clientId).orElse(null);
        List<Account> accounts = accountRepository.findAllByClient(client);
        model.addAttribute("client", client);
        model.addAttribute("accounts", accounts);
        return "account";
    }

}
