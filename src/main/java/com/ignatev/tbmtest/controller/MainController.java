package com.ignatev.tbmtest.controller;

import com.ignatev.tbmtest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("/")
public class MainController {

    private final ClientRepository clientRepository;

    public MainController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @GetMapping
    public ModelAndView main(ModelAndView modelAndView) {
        modelAndView.addObject("clients", clientRepository.findAll());
        modelAndView.setViewName("main");
        return modelAndView;
    }

}
