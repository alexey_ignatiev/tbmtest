package com.ignatev.tbmtest.repository;

import com.ignatev.tbmtest.entity.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface TransactionRepository extends CrudRepository<Transaction, UUID> {
}
