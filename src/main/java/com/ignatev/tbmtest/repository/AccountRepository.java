package com.ignatev.tbmtest.repository;

import com.ignatev.tbmtest.entity.Account;
import com.ignatev.tbmtest.entity.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface AccountRepository extends CrudRepository<Account, UUID> {
    List<Account> findAllByClient(Client client);
}
